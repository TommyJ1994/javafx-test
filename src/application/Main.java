package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;


public class Main extends Application
{
	@FXML
	private Button	myButton;
	@FXML
	private PasswordField myPassword;
	@FXML
	private TextField myText;

	public void start(Stage primaryStage) {
		try {
			AnchorPane root = (AnchorPane) FXMLLoader.load(Main.class.getResource("GUI.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Java FX 2.3");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void initialize() {
		assert myButton != null : "fx:id=\"myButton\" was not injected";
		assert myPassword != null : "fx:id=\"myPassword\" was not injected";
		assert myText != null : "fx:id=\"myText\" was not injected";

		handle();
	}

	@FXML
	private void handle() {
		myButton.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent event) {
				String username = myText.getText();
				System.out.println(username);
				
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}
}
